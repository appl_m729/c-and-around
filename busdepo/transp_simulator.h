#ifndef _TRANSP_SIMULATOR_H_
#define _TRANSP_SIMULATOR_H_

#include <cstddef>

using  std::size_t;

///////////////////////////////////////////////////////////////////////////////

class WorldSimulator {
public:

    WorldSimulator();
    ~WorldSimulator();

    static size_t MaxDuration() noexcept;

    /*! Apply next time step to the whole world inside the Simulator  */
    bool  doNextStep();

private:
    // TO DO:
    // 1) DepoLocation pointer
    // ..

};

///////////////////////////////////////////////////////////////////////////////

#endif   //  _TRANSP_SIMULATOR_H_

// TO DO:
// .. Ivan

// End of the file

