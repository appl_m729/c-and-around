#include <iostream>
#include "transp_simulator.h"
#include "transp_road.h"
#include "transp_dispatcher.h"
#include "transp_trolleybus.h"
#include "transp_depo.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

int main(int /*argc*/, char * /*argv*/ [])	{

    WorldSimulator * const ptr_world_simulator = new WorldSimulator();
	Road * const ptr_road = new Road();


    // .. setup

    // Main loop
    for (size_t i = 0; i < WorldSimulator::MaxDuration(); ++i) {

        if (!ptr_world_simulator->doNextStep()) {
            cout << "Simulation failed!.." << endl;
        }
    }

    delete  ptr_road;
    delete  ptr_world_simulator;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file



















//#include <trolleybus.h>
//#include <road.h>


//my_bus = Trolleybus(60, 45, 32.5);
//my_bus->drive(30, 2);


//my_road = Road(40, 1, 1, 5);
//std::cout << "Max load for the road is " << my_road->get_max_load(); << std::endl;
//if (my_road->is_asphalt()) {
//    std::cout << "There is asphalt here! It's definitely not Russia!" << std::endl;
//}
//else {
//    std::cout << "You are probably in Russia, babe!" << std::endl;
//}

//if (my_road->is_wired()) {
//    std::cout << "There are wires here!" << std::endl;
//}
//else {
//    std::cout << "Guess, asphalt also is absent))), Definitely Russia!" << std::endl;
//}

//std::cout << "Max load for the road is " << my_road->get_max_load(); << std::endl;

//if(my_road->stops_num() > 0) {
//    std::cout << "Ready for trolleybuses" << std::endl;
//}
