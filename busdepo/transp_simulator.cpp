#include <iostream>
#include "transp_simulator.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

WorldSimulator::WorldSimulator() {
	cout << "Constr world simulator" << endl;
}

WorldSimulator::~WorldSimulator() {
	cout << "Destr world simulator" << endl;
}

size_t WorldSimulator::MaxDuration() noexcept {

    // Can be modified

    return 1000;
}

bool WorldSimulator::doNextStep() {

    cout << "step number next... " << endl;

    return true;
}

// TO DO:
// ..

///////////////////////////////////////////////////////////////////////////////

// End of the file

