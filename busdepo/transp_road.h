#ifndef _TRANSP_ROAD_H_
#define _TRANSP_ROAD_H_

///////////////////////////////////////////////////////////////////////////////

class Road {
public:
	Road();
	~Road();
};

///////////////////////////////////////////////////////////////////////////////

#endif // _TRANSP_ROAD_H_

// End of file













/*
class Road {
public:
	bool is_asphalt() const;
	bool is_wired() const;
	int stops_num() const;
	int get_max_load() const;
	Road(int max_load, bool asphalt, bool wires, int stops_num);	
	~Road();
private:
	int max_load_; // weight limit for the road (for bridges, for example)
	bool asphalt_;
	bool wired_;
	int stops_; // trolleybus can drive here, if there are any stops on the road
};
*/
