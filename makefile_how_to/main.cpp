#include <iostream>
#include <complex>
#include <math.h>
#include <string>
#include "func_all.h"

using namespace std;


int main(){

    string int_str;
    int a[10];
    double x;
    complex<double> c;
    bool flag;
    int i;

    cout<<"Insert a string.\n";
    cin>>int_str;

    cout<<"Insert a real number in order to apply the euler's identity.\n";
    cin>>x;

    cout<<"Insert ten values for an array, in order to be sorted.\n";

    for (i = 0;i<10;i++){
        cin>>a[i];
    }

    c = ol_cmplx_exp(x);
    flag = sort_array(a);

    process(int_str);
    cout<<"Here is the result from Euler's identity: " << c << ".\n";

    if (flag==true){
        cout<<"Your array was sorted.\n";
    }
    else{
        cout<<"Your array was not sorted.\n";
    }

    return 0;
}
