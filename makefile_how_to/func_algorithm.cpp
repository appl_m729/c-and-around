#include <iostream>
#include <complex>

using namespace std;


bool sort_array(int array[10]){

    bool flag = true;

    int i,j,temp;

    j = 0;

    while (flag){
        flag = false;
        j++;

        for (i = 0; i<10 - j; i++){
            if (array[i] > array[i + 1]){
                temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
                flag = true;
            }
        }
    }

    return !flag;

}

