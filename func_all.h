#ifndef FUNC_ALL_H   /* Include guard */
#define FUNC_ALL_H

#include <iostream>
#include <complex>

//using namespace std;

using  std::complex;
using std::string;

void process (string inp_str); //Prints a String

complex<double> ol_cmplx_exp(double x); //Calculates the exponential form of a complex number

bool sort_array(int array[10]); //Returns true if the matrix is sorted correctly

#endif
